# django-workspace\requirements.txt

# Django
Django>=2.0,<3.0

# PostgreSQL
psycopg2>=2.7,<3.0

# Developement
django-extensions